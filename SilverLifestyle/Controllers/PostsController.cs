﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using SilverLifestyle.Models;

namespace SilverLifestyle.Controllers
{
    public class PostsController : Controller
    {
        private SilverEntityEntities db = new SilverEntityEntities();

        // GET: Posts
        public ActionResult Index()
        {
            return View(db.Posts.ToList());
        }

        // GET: Posts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // GET: Posts/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "PostId,PostDate,PostText,PostImage,PosterId")] Post post, HttpPostedFileBase postedFile)
        {
            ModelState.Clear();         // Required since returning a view in POST method 

            // Set the Poster ID to the User's ID in the authentication table
            post.PosterId = User.Identity.GetUserId();

            // Set the current date
            post.PostDate = DateTime.Now;

            // If a file has been attached
            if (postedFile != null)
            {
                // Define the path to the image
                var uniqueFileName = string.Format(@"{0}", Guid.NewGuid());
                post.PostImage = uniqueFileName;                            // Set current path to image in post
            }
            
            TryValidateModel(post);

            if (ModelState.IsValid)
            {
                // If a file has been attached
                if (postedFile != null)
                {
                    string serverPath = Server.MapPath("~/Uploads/");   // Define path to images on server
                    string fileExtension = System.IO.Path.GetExtension(postedFile.FileName);    // Define file extension
                    string filePath = post.PostImage + fileExtension;   // Define path to file with extension
                    post.PostImage = filePath;                          // Add image path with extension
                    postedFile.SaveAs(serverPath + post.PostImage);     // Save the file in specified server storage
                }
                
                db.Posts.Add(post);                     // Add the new post object to the db

                db.SaveChanges();                       // Save the changes to the db
                return RedirectToAction("Index");       // Send user back to index page (view)
            }

            return View(post);
        }

        // GET: Posts/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "PostId,PostDate,PostText,PostImage,PosterId")] Post post, HttpPostedFileBase editpostedFile)
        {
            var currentPost = db.Posts.FirstOrDefault(p => p.PostId == post.PostId);
            if (currentPost == null)
            {
                return HttpNotFound();
            }

            // If a file has been attached
            if (editpostedFile != null)
            {
                // Define the path to the image
                var uniqueFileName = string.Format(@"{0}", Guid.NewGuid());
                post.PostImage = uniqueFileName;                    // Set current path to image in post
                string serverPath = Server.MapPath("~/Uploads/");   // Define path to images on server
                string fileExtension = System.IO.Path.GetExtension(editpostedFile.FileName);    // Define file extension
                string filePath = post.PostImage + fileExtension;   // Define path to file with extension

                currentPost.PostImage = filePath;                   // Add image path with extension to update
                editpostedFile.SaveAs(serverPath + currentPost.PostImage);     // Save the file in specified server storage
            }

            if (ModelState.IsValid)
            {
                currentPost.PostText = post.PostText;
                currentPost.PostImage = post.PostImage;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(post);
        }

        // GET: Posts/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
