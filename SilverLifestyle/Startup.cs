﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SilverLifestyle.Startup))]
namespace SilverLifestyle
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
